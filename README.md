# Tickermaster User API

Spring Boot Restful API with MySql database based on Swagger documentation. The web app and the databse run in Docker containers.

## Build

To build simply run the following command:

```
./mvnw install
```
This will generate code based on the Swagger documentation and create our jar files.

## Create Docker image of our app

From the root directory run:

```
docker image build -t userapi .
```

## Run the System

We can run the whole thing with docker-compose:

```
docker-compose up
```
Docker will pull the MySQL and userapi images.

## Testing the app

The endpoints should be available @ http://localhost:8080/users/

Create a new user:

```
curl -X 'POST' \
  'http://localhost:8080/users/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "email": "test@test.com",
  "firstName": "John",
  "lastName": "Doe",
  "address": {
    "address": "4, Privet Drive",
    "city": "Little Whinging",
    "country": "Surrey, England",
    "zipcode": "G12B12"
  }
}'
```

Search for users:

By passing in the appropriate the user id (uuid) in the path param, you can get a user details:

```
curl -X 'GET' \
  'http://localhost:8080/users/885b1e03-75d2-4e71-87f6-5b06ce22c139' \
  -H 'accept: application/json'
```

By passing in the keyword option, you can search for users by name or email:

```
curl -X 'GET' \
  'http://localhost:8080/users?keyword=bill' \
  -H 'accept: application/json'
```
