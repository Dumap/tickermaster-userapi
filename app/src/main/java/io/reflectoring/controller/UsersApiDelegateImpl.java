package io.reflectoring.controller;

import io.reflectoring.api.UsersApiDelegate;
import io.reflectoring.service.UserService;
import io.reflectoring.model.CreateUser;
import io.reflectoring.model.GetUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UsersApiDelegateImpl implements UsersApiDelegate{

    private final UserService userService;

    @Autowired
    public UsersApiDelegateImpl(UserService userService){
        this.userService = userService;
    }

    @Override
    public ResponseEntity<GetUser> getUserById(UUID id){

        return ResponseEntity.ok(userService.getUserById(id));
    }

    @Override
    public ResponseEntity<GetUser> createUser(CreateUser createUser) {

        GetUser user = userService.createUser(createUser);
        return ResponseEntity.ok(user);
    }

    @Override
    public ResponseEntity<List<GetUser>> searchUsers(String keyword,
                                                     Integer offset,
                                                     Integer limit) {
        return ResponseEntity.ok(userService.searchUsers(keyword, offset, limit));
    }
}
