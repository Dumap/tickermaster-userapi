package io.reflectoring.repository;

import io.reflectoring.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, Long> {
    User findById(UUID id);
    List<User> findByEmailOrFirstNameOrLastName(String email, String firstName, String lastName);
}