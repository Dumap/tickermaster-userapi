package io.reflectoring.service;

import io.reflectoring.model.CreateUser;
import io.reflectoring.model.GetUser;

import java.util.List;
import java.util.UUID;

public interface UserService {

    GetUser getUserById(UUID id);

    GetUser createUser(CreateUser creatUser);

    List<GetUser> searchUsers(String keyword, Integer offset, Integer limit);
}
