package io.reflectoring.service;

import io.reflectoring.model.*;
import io.reflectoring.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private  UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public GetUser getUserById(UUID id){
        User result = userRepository.findById(id);

        return modelMapper.map(result, GetUser.class);
    }

    @Override
    public GetUser createUser(CreateUser creatUser) {

        User user = new User();
        user.setEmail(creatUser.getEmail());
        user.setFirstName(creatUser.getFirstName());
        user.setLastName(creatUser.getLastName());
        Address address = new Address();
        address.setAddress(creatUser.getAddress().getAddress());
        address.setCity(creatUser.getAddress().getCity());
        address.setCountry(creatUser.getAddress().getCountry());
        address.setZipcode(creatUser.getAddress().getZipcode());
        address.setUser(user);
        user.setAddress(address);

        userRepository.save(user);

        return modelMapper.map(user, GetUser.class);
    }

    //Todo: implement offset and limit
    @Override
    public List<GetUser> searchUsers(String keyword,
                                      Integer offset,
                                      Integer limit) {

        List<User> users = new ArrayList<>();
        if(keyword == null)
            userRepository.findAll().forEach(users::add);
        else
            users.addAll(userRepository.findByEmailOrFirstNameOrLastName(keyword, keyword, keyword));

        return users
                .stream()
                .map(user -> modelMapper.map(user, GetUser.class))
                .collect(Collectors.toList());
    }
}
