package io.reflectoring.controller;

import io.reflectoring.model.CreateUser;
import io.reflectoring.model.GetUser;
import io.reflectoring.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UsersApiDelegateImplTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UsersApiDelegateImpl usersApiDelegate;

    private final UUID uuid = UUID.fromString("8421b1b3-1f3f-4655-b950-8f68c196f06a");
    private GetUser gUser;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        gUser = new GetUser();
        gUser.setEmail("jim@test.com");
        gUser.setFirstName("jim");
        gUser.setLastName("jones");
    }

    @Test
    void testGetUserById() {
        //given
        when(userService.getUserById(any(UUID.class))).thenReturn(gUser);

        //calling method under the test
        ResponseEntity<GetUser> result = usersApiDelegate.getUserById(uuid);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.hasBody()).isTrue();

        //verify that service was called
        verify(userService, times(1)).getUserById(uuid);
    }

    @Test
    void testCreateUser() {
        //given
        CreateUser cUser = new CreateUser();
        when(userService.createUser(any(CreateUser.class))).thenReturn(gUser);

        //calling method under the test
        ResponseEntity<GetUser> result = usersApiDelegate.createUser(cUser);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.hasBody()).isTrue();

        //verify that service was called
        verify(userService, times(1)).createUser(any(CreateUser.class));
    }

    @Test
    void testSearchUsersNoKeyword() {
        //given
        List<GetUser> users = new ArrayList<>();
        users.add(gUser);
        when(userService.searchUsers(any(), any(), any())).thenReturn(users);

        //calling method under the test
        ResponseEntity<List<GetUser>> result = usersApiDelegate.searchUsers(null, null, null);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.hasBody()).isTrue();

        //verify that service was called
        verify(userService, times(1)).searchUsers(any(), any(), any());
    }

    @Test
    void testSearchUsersWithKeyword() {
        //given
        String keyword = "jim";
        List<GetUser> users = new ArrayList<>();
        users.add(gUser);
        when(userService.searchUsers(any(), any(), any())).thenReturn(users);

        //calling method under the test
        ResponseEntity<List<GetUser>> result = usersApiDelegate.searchUsers(keyword, 1, 2);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(result.hasBody()).isTrue();

        //verify that service was called
        verify(userService, times(1)).searchUsers(any(), any(), any());
    }

    @Test
    void testSearchUsersNoResults() {
        //given
        String keyword = "jim";
        List<GetUser> users = new ArrayList<>();
        when(userService.searchUsers(any(), any(), any())).thenReturn(users);

        //calling method under the test
        ResponseEntity<List<GetUser>> result = usersApiDelegate.searchUsers(keyword, 1, 2);

        assertThat(result.getStatusCodeValue()).isEqualTo(200);
        assertThat(Objects.requireNonNull(result.getBody()).isEmpty()).isTrue();

        //verify that service was called
        verify(userService, times(1)).searchUsers(any(), any(), any());
    }
}