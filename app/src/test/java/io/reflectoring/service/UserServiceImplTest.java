package io.reflectoring.service;

import io.reflectoring.model.Address;
import io.reflectoring.model.CreateUser;
import io.reflectoring.model.GetUser;
import io.reflectoring.model.User;
import io.reflectoring.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private UserServiceImpl userService;

    private final UUID uuid1 = UUID.fromString("8421b1b3-1f3f-4655-b950-8f68c196f06a");
    private final User u1 = new User("bill@test.com", "bill", "doe", new Address("55 main","Boston","US","12345"));

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        u1.setId(uuid1);
    }

    @Test
    void testGetUserById() {
        //given
        ModelMapper testMapper = new ModelMapper();
        GetUser gUser = testMapper.map(u1, GetUser.class);
        when(userRepository.findById(any(UUID.class))).thenReturn(u1);
        when(modelMapper.map(any(), any())).thenReturn(gUser);

        //calling method under the test
        GetUser result = userService.getUserById(uuid1);

        //assert
        assertThat(result.getId()).isEqualTo(uuid1);
        assertThat(result.getEmail()).isEqualTo("bill@test.com");

        //verify that repository was called
        verify(userRepository, times(1)).findById(uuid1);
    }

    @Test
    void testCreateUser() {
        //given
        ModelMapper testMapper = new ModelMapper();
        CreateUser cUser = testMapper.map(u1, CreateUser.class);
        GetUser gUser = testMapper.map(u1, GetUser.class);
        when(userRepository.save(any(User.class))).thenReturn(u1);
        when(modelMapper.map(any(), any())).thenReturn(gUser);

        //calling method under the test
        GetUser result = userService.createUser(cUser);

        //assert
        assertThat(result.getId()).isEqualTo(uuid1);
        assertThat(result.getEmail()).isEqualTo("bill@test.com");

        //verify that repository was called
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void testSearchUsersNoKeyword() {
        ModelMapper testMapper = new ModelMapper();
        GetUser gUser = testMapper.map(u1, GetUser.class);
        List<User> users = new ArrayList<>();
        users.add(u1);
        when(userRepository.findAll()).thenReturn(users);
        when(modelMapper.map(any(), any())).thenReturn(gUser);

        //calling method under the test
        List<GetUser> results = userService.searchUsers(null, null, null);

        //assert
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getEmail()).isEqualTo("bill@test.com");

        //verify that repository was called
        verify(userRepository, times(1)).findAll();

    }

    @Test
    void testSearchUsersWithKeyword() {
        ModelMapper testMapper = new ModelMapper();
        String keyword = "bill";
        GetUser gUser = testMapper.map(u1, GetUser.class);
        List<User> users = new ArrayList<>();
        users.add(u1);
        when(userRepository.findByEmailOrFirstNameOrLastName(keyword,keyword,keyword)).thenReturn(users);
        when(modelMapper.map(any(), any())).thenReturn(gUser);

        //calling method under the test
        List<GetUser> results = userService.searchUsers(keyword,1,2);

        //assert
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getEmail()).isEqualTo("bill@test.com");

        //verify that repository was called
        verify(userRepository, times(1)).findByEmailOrFirstNameOrLastName(keyword,keyword,keyword);

    }
}